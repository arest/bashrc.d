#!/bin/bash

function make_link {
    local fpath="$1"
    local flink="$2"
    local Fpath="$HOME/$1"
    local Flink="$HOME/$2"

    if [ -L "$Fpath" ]; then
        echo -n "Link to $fpath exist: "
        ls -l $fpath
        return 50 
    else
        if [ -f "$Fpath" ]; then
            if [ -s "$Fpath" ]; then
                echo "Regular file $Fpath exist."
                echo "Renaming to $fpath.bak.$fdate"
                local fdate=$(date +%Y%m%d%H%M%S)
                if [ -O "$Fpath" ] && [ -w "$Fpath" ]; then
                    mv $fpath $fpath.bak.$fdate && echo "Rename completed." || echo "Rename failed: $?"
                else
                    echo "$fpath: You are not owner or not enough permissions to modify the file."
                    echo "$fpath: File unchanged."
                    return 2
                fi
            else
                echo "File $fpath is zero size. Removing..."
                [ -O "$Fpath" ] && rm $Fpath && echo "done." || ( echo "Removing failed."; return 2 )
            fi
        fi
    fi
    cd ~
    ln -s $flink $fpath && echo "Link $fpath created to $flink" ||  ( echo "Link creation failed."; return 3 )
    return 0
}

function make_dirlink () {
    local fpath="$1"
    local flink="$2"
    local Fpath="$HOME/$1"
    local Flink="$HOME/$2"
    
    if [ -L "$Fpath" ]; then
        echo "Link to $Fpath exist: "
        ls -l $Fpath
        return 50
    else
        if [ -d "$Fpath" ]; then
            echo "Directyory $Fpath exist. Exiting"
            exit 1
        fi
    fi
    cd ~
    ln -s $flink $fpath && echo "Link $fpath created successfully." || echo "Link $fpath creation failed."
}
    

if [ ! -d $HOME/Dropbox ]; then 
    echo "Dropbox folder not found. Exiting..."
    exit 1
fi


declare -A CFG CFGD
CFGD[SCRIPT]=Dropbox/SCRIPT
CFGD[.bashrc.d]=SCRIPT/.bashrc.d
CFG[.bashrc]=SCRIPT/.bashrc
CFG[.profile]=SCRIPT/.profile
CFG[.vimrc]=SCRIPT/.vimrc
CFG[.muttrc]=SCRIPT/.muttrc
CFG[.ssh/config]=SCRIPT/.ssh/config
CFG[.config/terminator/config]=SCRIPT/.config/terminator/config

for fname in ${!CFGD[@]}; do
    echo "$fname: creating link to ${CFGD[$fname]}"
    make_dirlink $fname ${CFGD[$fname]}
    case $? in
        0)          echo "Done." ;;
        1|2)        echo "Failed!" ;;
        50)         echo "Skip.";;
    esac
done

for fname in ${!CFG[@]}; do
    echo "$fname: creating link to ${CFG[$fname]} ... "
    make_link $fname ${CFG[$fname]}
    case $? in
       0)          sleep 0.5 ; echo "Done.";; 
     1|2)          leep 0.5 ; echo "Failed!";;
      50)          echo "Skip..."
    esac
done
