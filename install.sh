TARGET=${1:-$HOME/.bashrc}
if [ ! -f "$TARGET" ]; then
	echo "File $1 does not exist."
	echo "Usage: install.sh [ -a ] [ TARGET_BASHRC_FILE ]"
	echo "Installation failed."
	exit 1
fi

echo Verifying python-pygments installation...
dpkg -l |grep python-pygments >/dev/null || sudo apt install python-pygments > /dev/null && echo done.

echo -n "Updating ${TARGET} ... "
grep 'BASHRC_INIT' $TARGET >/dev/null || if [ -f ./start_up.sh ]; then cat ./start_up.sh >> $TARGET; else echo "start_up.sh not found. Installation failed."; exit; fi    
echo -e "completed."
echo -n "Verifying installation..."
source $TARGET
echo "Everything OK? Press Ctrl-C to abort installation."
read
if [ $? -ne 0 ]; then
	echo -e " failed."
	echo -n "Uninstalling..."
	head -n -2 $TARGET && echo -e " done."
	exit 1
fi
echo -e " done."

#[ ! -L $HOME/.bashrc.d ] &&  ln -s $basepath ~/.bashrc.d 
#if [ -f $HOME/.bashrc ]; then
#	cp $HOME/.bashrc $HOME/.bashrc.bak
#	cp ./.bashrc $HOME
#fi
#
#if [ -f $HOME/.bash_profile ]; then
#	cp $HOME/.bash_profile $HOME/.bash_profile.bak
#	cp ./.bash_profile $HOME
#fi
