BWhite='\e[1;37m'       # White
On_Red='\e[41m'         # Red
BBlue='\e[1;34m'        # Blue
BRed='\e[1;31m'         # Red Bold
COL_ALERT=${BWhite}${On_Red} # Bold White on red background
NC="\e[m"               # Color Reset
if [ -f ${BASHRC_DIR:="$HOME/.bashrc.d"}/bashrc.conf ]; then
    . $BASHRC_DIR/bashrc.conf
fi

echo -n " . .bashrc: "
[ $BASHRC_DEBUG ] && { echo; echo -en "$COL_ALERT DEBUG MODE is ENABLED!$NC"; }
[ $BASHRC_DEBUG ] && echo -en "$COL_ALERT DEBUG MODE is ENABLED!$NC"
#BASHRC_DEBUG=1
files=( $(find ${BASHRC_DIR}/ -maxdepth 1 -type f -regex '.*/[0-9][0-9].bashrc\..*$') )
files=( ${files[@]##*/} )
newfiles=( $(echo "${files[@]}" | tr ' ' '\n' | sort |tr '\n' ' ') )
#echo ${newfiles[@]}
#for file in $(find ${BASHRC_DIR}/ -maxdepth 1 -type f -regex '.*/[0-9][0-9].bashrc\..*$' |sed 's|^.*/\([^/]*\)$|\1|g'|sort) ; do
for file in ${newfiles[@]}; do
	fullfile=$BASHRC_DIR/$file
	[ $BASHRC_DEBUG ] && { echo; echo -n "found: $file - "; }
	[ $BASHRC_DEBUG ] && echo "f=$file   file=$fullfile"
#	if [ -d ${f} ]; then
	[ $BASHRC_DEBUG ] && echo "Preparing $file ..."
	case ${file} in
			[0-9][0-9].bashrc.*.common)
								[ $BASHRC_DEBUG ] && echo "Select 1"
								if [ -f "$fullfile" -a -x "$fullfile" ]; then
		       						[ $BASHRC_DEBUG ] && echo -en "${COL_ALERT}exec common ${NC}"
									. $fullfile;
								fi
					#			break
								;;
			[0-9][0-9].bashrc.*)
								[ $BASHRC_DEBUG ] && echo "Select 2"
								if [ -f "$fullfile" -a -x "$fullfile" ]; then
									[ $BASHRC_DEBUG ] && echo -en "${COL_ALERT}exec ${NC}"
									. $fullfile
								fi
					#			break
								;;
			[0-9][0-9].bashrc.*.local.$HOSTNAME)
								[ $BASHRC_DEBUG ] && echo "Select 3"
								if [ -f "$fullfile" -a -x "$fullfile" ]; then
									[ $BASHRC_DEBUG ] && echo -en "${COL_ALERT}exec local $HOSTNAME ${NC}"
									. $fullfile
								fi
					#			break
								;;
			[0-9][0-9].bashrc.*.private)
								[ $BASHRC_DEBUG ] && echo "Select 4"
								if [ -f "$fullfile" -a -x "$fullfile" ]; then
									[ $BASHRC_DEBUG ] && echo -en "${COL_ALERT}exec private $HOSTNAME ${NC}"
									. $fullfile
								fi
#								break
								;;

			*)
							[ $BASHRC_DEBUG ] && echo "Select 5"
								[ $BASHRC_DEBUG ] && echo -n skip
								;;
	esac
done
