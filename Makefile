all:
	echo "Available targets to make:\n\tmake install:\t - put .bashrc.d initialization code into $HOME/.bashrc\n\tmake distclean:\t - remove installation part from $HOME/.bashrc\n\tmake update:\t - update project via git"
install:
	./install.sh
distclean:
	echo "Yet not implemented"
update:
	git pull origin
