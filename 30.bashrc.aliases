BASHRC_NODE=$BASH_SOURCE;. $BASHRC_DIR/head.sh
#
# cd alias
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

alias ls='/bin/ls'
alias l='ls -l'
alias ll="ls -Alh"
alias L="ls -AlFqh --group-directories-first"
alias lh="ls -Alh"
alias lR="ls -AlhR"
alias lt="ls -Alhrt"
alias lS="ls -AlFS"

alias mkdir='mkdir -pv'

alias h='history'
alias j="jobs -l"
alias g='egrep'
alias g5='egrep -C5'
alias g3='egrep -C3'
alias ack='ack-grep --color'
alias Ack='ack-grep --passthru --color '

if [ -x $(which colordiff) ]; then alias diff='colordiff'; fi

alias x=exit
alias screen="screen -U"
alias s=screen
alias sha1='openssl sha1'
alias SU='sudo su -m'
alias su='sudo su -m'

alias pu="pushd"
alias po="popd"

alias today='date +%F'
alias NOW='date "+%Y%m%d-%H%M%S"'

alias ef='tail -f '
alias wd='ps -aux '
alias df='df -kTh'
alias csort='sort | uniq -c | sort -n' # column sort piped data
alias hg='history | grep '
alias psg='ps aux |grep '
alias wdg='ps aux |grep '
#alias m='less -FRX' 			# F=exit at eof, R=display ANSI color chars
alias m='most -w'
#alias m='less -FrX' 			# F=exit at eof, R=display ANSI color chars
#alias mc='less -FrXS' 			# S=crop long strings
alias mc='most'
alias less='most -w'
alias lm='l |m'

alias tree='tree -Csuh'			# C=color, s=size, u=user, h=human readable

alias cdtmp='pushd $(mktemp -d)' 	# creates a temp dir and cds into it
alias active='grep -v -e "^$" -e"^ *#"' # shows only lines that are not blank or commented out

alias logs="find /var/log -type f -exec file {} \; | grep 'text' | cut -d' ' -f1 | sed -e's/:$//g' | grep -v '[0-9]$' | xargs tail -f" # Tail all logs in /var/log

# two handy single-letter aliases
#alias u='ls -hltr'
#alias e='du * -cs | sort -nr | head'

# The 'folders' alias is great! I modified it slightly so that
# directories with spaces don't cause errors.
#alias folders='find . -maxdepth 1 -type d -print0 | xargs -0 du -sk | sort -rn'

#lsnet(){
#        lsof -i  | awk '{printf("%-14s%-20s%s\n", $10, $1, $9)}' | sort
#}

alias RM='rm -rf'
alias Screen1='screen -O -U -S arest-1'
alias Screen2='screen -O -U -S arest-2'
alias Screen3='screen -O -U -S arest-3'
alias Screen4='screen -O -U -S arest-4'
alias trace='traceroute'
alias My='chown -R arest:arest '
alias t='telnet'
#alias ec2='echo ""; echo "ec2-107-20-23-7.compute-1.amazonaws.com";echo ""; host ec2-107-20-23-7.compute-1.amazonaws.com'

alias gR='grep -r'
alias pz='pygmentize'
alias pg='pygmentize'
alias dusk='du -sk *|sort -n'
alias vialias='vim ~/.bashrc.d/30.bashrc.aliases'
alias clip-dsa='xclip -sel clip < ~/.ssh/id_dsa.pub'
alias clip-rsa='xclip -sel clip < ~/.ssh/id_rsa.pub'
alias tailf='tail -f'
alias vlans='cat /proc/net/vlan/config'

alias agu='sudo apt update'
alias agi='sudo apt install'
alias agd='sudo apt dist-upgrade'
alias ags='sudo aptitude search'
alias agsh='sudo apt-cache show'
alias afs='sudo apt-file search'
alias afsh='sudo apt-file show'
alias afu='sudo apt-file update'
alias Z-list='sudo /sbin/zfs list -o name,avail,mounted,mountpoint,sharesmb,used,usedds'
alias Vhosts='apachectl -t -D DUMP_VHOSTS'
#alias logall='tail -f /var/log/all.log'
alias logall='tail -f /var/log/messages'
alias logsys='tail -f /var/log/syslog'
alias logmail='tail -f /var/log/mail.log'
alias logauth='tail -f /var/log/auth.log'
alias loglast='watch -d "ls -lt /var/log |head -30"'
alias logbind='tail -f /var/log/named.log'
alias lognamed='tail -f /var/log/named.log'
alias logwww='LOG=/var/log/apache2; [ `which multitail` ] && multitail $LOG/access.log -I $LOG/error.log -I $LOG/*access.log -I $LOG/*error.log || tail -f $LOG/access.log $LOG/error.log $LOG/*access.log $LOG/*error.log'
alias logweb='logwww'
alias logapache2='logwww'
alias logdhcp='tail -f /var/log/dhcpd.log'
alias fu='/home/arest/GIT/fu/fu'
alias ldif-import='sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f '
alias LDAP-listdn='ldapsearch -x -LLL -H ldap:/// -b dc=arest-home,dc=pp,dc=ua dn'
alias curlx='curl -v --socks5-hostname localhost:9050'
alias urldecode='python -c "import sys, urllib as ul; print ul.unquote_plus(sys.argv[1])"'
alias urlencode='python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])"'
alias su='su -m'
alias Sstat='systemctl status'
alias Sre='systemctl restart'
alias Sstart='systemctl start'
alias Sstop='systemctl stop'
alias S='systemctl'
alias J='journalctl'
alias Jx='journalctl -xe'
alias jxe='journalctl -xe'
alias git-push='git add .; git commit; git push -u origin master'
alias git-pull='git pull origin master'
alias pkcs11-tool.eToken='pkcs11-tool --module=/usr/lib/libeTPkcs11.so'
alias pkcs11-tool.ruToken='pkcs11-tool --module=/usr/lib/librtpkcs11ecp.so'
alias sslcat='openssl x509 -noout -text -in '
alias x509='openssl x509 -noout -text -in '
