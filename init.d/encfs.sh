PATH=/sbin:/usr/sbin:/bin:/usr/bin

. ~/.bashrc.d/init.d/encfs.conf
. /lib/init/vars.sh
. /lib/lsb/init-functions

do_start() {
        if [ -d "$SRCENCDIR" ]; then
                [ "$VERBOSE" != no ] && log_begin_msg "Starting encfs mount"
				/usr/bin/encfs $SRCENCDIR $DSTENCDIR
                ES=$?
                [ "$VERBOSE" != no ] && log_end_msg $ES
                return $ES
        fi
}

do_stop() {
		if [ -d "$DSTENCDIR" ]; then
            [ "$VERBOSE" != no ] && log_begin_msg "Syncing disks..."
			/bin/sync
            [ "$VERBOSE" != no ] && log_begin_msg " unmounting $DSTENCDIR"
			/bin/fusermount -u $DSTENCDIR
			ES=$?
            [ "$VERBOSE" != no ] && log_end_msg $ES
            return $ES
		fi
}
case "$1" in
    start)
        do_start
        ;;
    restart|reload|force-reload)
        echo "Error: argument '$1' not supported" >&2
        exit 3
        ;;
    stop)
		do_stop
        ;;
    status)
		local marker='encrypteddisk.###'
		if [ -f "$DSTENCDIR/$marker" ]; then
			log_success_msg "Folder $DSTENCDIR is mounted"
			exit 0
		else
			log_failure_msg "Folder $DSTENCDIR not mounted or marker file $DSTENCDIR/$marker is lost"
	        exit 3
		fi
        ;;
    *)
        echo "Usage: $0 start|stop|status" >&2
        exit 3
        ;;
esac

